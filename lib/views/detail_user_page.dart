



import 'package:app_users/controller/data_base_helper.dart';
import 'package:app_users/views/list_user_page.dart';
import 'package:flutter/material.dart';

class Detail extends StatefulWidget {
  List list;
  int index;
  Detail({this.index, this.list});

  @override
  _DetailState createState() => _DetailState();
}

class _DetailState extends State<Detail> {
    DataBaseHelper databaseHelper = new DataBaseHelper();

  _navigateList(BuildContext context) async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ListUser()),
    );

    if (result) {
      setState(() {});
    }
  }

  //create function delete
  void confirm() {
    AlertDialog alertDialog = AlertDialog(
      content:
          Text("Esta seguto de eliminar '${widget.list[widget.index]['idUsuario']}'"),
      actions: <Widget>[
        RaisedButton(
          child: Text(
            "OK remove!",
            style: TextStyle(color: Colors.black),
          ),
          color: Colors.red,
          onPressed: () {
            setState(() {
              
                   databaseHelper.deleteUser(widget.list[widget.index]['idUsuario']);
                   _navigateList(context);
            });
          },
        ),
        RaisedButton(
          child: Text("CANCEL", style: TextStyle(color: Colors.black)),
          color: Colors.green,
          onPressed: () => Navigator.pop(context),
        ),
      ],
    );

    showDialog(context: context, child: alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      // appBar:
      //     new AppBar(title: new Text("${widget.list[widget.index]['name']}")),
      body: Container(
        height: 340.0,
        padding: const EdgeInsets.all(20.0),
        child: Card(
          child: Center(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                Text(
                  ' ${widget.list[widget.index]['idUsuario']}',
                  style: TextStyle(fontSize: 20.0),
                ),
                Text(
                  ' ${widget.list[widget.index]['nombeUsuario']}  ${widget.list[widget.index]['apellidoUsuario']}',
                  style: TextStyle(fontSize: 20.0),
                ),
                Divider(),
                Text(
                  "Tel : ${widget.list[widget.index]['telefonoUsuario']}",
                  style: TextStyle(fontSize: 18.0),
                ),
                Divider(),
                Text(
                  "Email : ${widget.list[widget.index]['emailUsuario']}",
                  style: TextStyle(fontSize: 18.0),
                ),
                 Divider(),
                Text(
                  "Direccion : ${widget.list[widget.index]['direccionUsuario']}",
                  style: TextStyle(fontSize: 18.0),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30.0),
                ),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    RaisedButton(
                        child: Text("Edit"),
                        color: Colors.blueAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30.0)),
                        onPressed: () {}),
                    VerticalDivider(),
                    RaisedButton(
                      child: Text("Delete"),
                      color: Colors.redAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30.0)),
                      onPressed: (){
                        confirm();
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
