import 'package:app_users/controller/data_base_helper.dart';
import 'package:flutter/material.dart';

class AddDataUser extends StatefulWidget {
  AddDataUser({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddDataUserState createState() => _AddDataUserState();
}

class _AddDataUserState extends State<AddDataUser> {
  DataBaseHelper databaseHelper = new DataBaseHelper();

  final TextEditingController nombreController = new TextEditingController();
  final TextEditingController apellidoController = new TextEditingController();
  final TextEditingController emailController = new TextEditingController();
  final TextEditingController telController = new TextEditingController();

  final TextEditingController direccionController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Add Product',
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text('Add Product'),
        // ),
        body: Container(
          child: ListView(
            padding: const EdgeInsets.only(
                top: 62, left: 12.0, right: 12.0, bottom: 12.0),
            children: <Widget>[
              Container(
                height: 50,
                child: new TextField(
                  controller: nombreController,
                  onChanged: (value) {
                    print(value);
                    databaseHelper.userInstance.nombeUsuario = value;
                  },
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: 'nombre',
                    hintText: 'Nombre de usuario',
                    icon: new Icon(Icons.email),
                  ),
                ),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: apellidoController,
                  onChanged: (value) {
                    print(value);
                    databaseHelper.userInstance.apellidoUsuario = value;
                  },
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Apellido',
                    hintText: 'Apellido de usuario',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: telController,
                  onChanged: (value) {
                    print(value);
                    databaseHelper.userInstance.telefonoUsuario = value;
                  },
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Tel',
                    hintText: 'Telefono usuario',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new TextField(
                  controller: emailController,
                  onChanged: (value) {
                    print(value);
                    databaseHelper.userInstance.emailUsuario = value;
                  },
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Email',
                    hintText: 'Email usuario',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new TextField(
                  onChanged: (value) {
                    print(value);
                    databaseHelper.userInstance.direccionUsuario = value;
                  },
                  controller: direccionController,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    labelText: 'Direccion',
                    hintText: 'Direccion usuario',
                    icon: new Icon(Icons.vpn_key),
                  ),
                ),
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 44.0),
              ),
              Container(
                height: 50,
                child: new RaisedButton(
                  onPressed: () {
                    setState(() {
                      databaseHelper.addDataUser();
                      databaseHelper.getData();
                    Navigator.pop(context, true);
                    });
                  },
                  color: Colors.blue,
                  child: new Text(
                    'Add',
                    style: new TextStyle(
                        color: Colors.white, backgroundColor: Colors.blue),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
