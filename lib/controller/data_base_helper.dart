
import 'dart:convert';

import 'package:app_users/model/User.dart';
import 'package:http/http.dart'as http;


class DataBaseHelper{
User userInstance =User();
DataBaseHelper(){
  userInstance=User();

}

//List User
 Future<List> getData() async {
    final response = await http.get("http://192.168.1.4:8182/user");
    print(response.body);
    return json.decode(response.body);
  }


  //Funcion para agregar un usuario a la BD
  Future<http.Response> addDataUser() async {
    var url = 'http://192.168.1.4:8182/user';
    //encode Map to JSON
    var body = json.encode(userInstance);
    print("------->$userInstance");

    var response = await http.post(url,headers: {"Content-Type": "application/json"}, body: body).then((value) {
      getData() ;
    });

    print("${response.statusCode}");
    print("${response.body}");
    return response;
  }

  //Funcion  para eliminar
  Future<void> deleteUser(int id) async {
    var url = 'http://192.168.1.4:8182/user/$id';
    //encode Map to JSON
    print("------->$userInstance");
    var request = await http.delete(url);
    print("${request.statusCode}");
    print("${request.body}");
    return request;
  }

}

