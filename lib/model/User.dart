class User {
  static User singleton;
  int idUsuario;
  String nombeUsuario;
  String apellidoUsuario;
  String emailUsuario;
  String telefonoUsuario;
  String direccionUsuario;

  User.initial(
      {this.idUsuario,
      this.nombeUsuario,
      this.apellidoUsuario,
      this.emailUsuario,
      this.telefonoUsuario,
      this.direccionUsuario});

  factory User() {
    if (singleton == null) {
      singleton = User.initial();
    }
    return singleton;
  }

  User.fromJson(Map<String, dynamic> json) {
    idUsuario = json['idUsuario'];
    nombeUsuario = json['nombeUsuario'];
    apellidoUsuario = json['apellidoUsuario'];
    emailUsuario = json['emailUsuario'];
    telefonoUsuario = json['telefonoUsuario'];
    direccionUsuario = json['direccionUsuario'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idUsuario'] = this.idUsuario;
    data['nombeUsuario'] = this.nombeUsuario;
    data['apellidoUsuario'] = this.apellidoUsuario;
    data['emailUsuario'] = this.emailUsuario;
    data['telefonoUsuario'] = this.telefonoUsuario;
    data['direccionUsuario'] = this.direccionUsuario;
    return data;
  }
}
