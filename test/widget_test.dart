// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.


import 'package:flutter_test/flutter_test.dart';

import 'package:http/http.dart' as http;

import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';


class MockClient extends Mock implements http.Client {}

@GenerateMocks([http.Client])
void main() {
  testWidgets('Counter increments smoke test', (WidgetTester tester) async {
    final client = MockClient();
   when(client.get('http://192.168.1.4:8182/user'))
          .thenAnswer((_) async => http.Response('{"title": "Test"}', 200));
  });
}
